/* main.c -- MQTT client example
*
* Copyright (c) 2014-2015, Tuan PM <tuanpm at live dot com>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* * Redistributions of source code must retain the above copyright notice,
* this list of conditions and the following disclaimer.
* * Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the distribution.
* * Neither the name of Redis nor the names of its contributors may be used
* to endorse or promote products derived from this software without
* specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ets_sys.h"
#include "driver/uart.h"
#include "osapi.h"
#include "mqtt.h"
#include "wifi.h"
#include "config.h"
#include "debug.h"
#include "gpio.h"
#include "user_interface.h"
#include "mem.h"


MQTT_Client mqttClient;
sint16 size_buf = 0;
uint8 bssid[6];
char moduleID[17];
uint16 b_pres_time = 0;
uint16 data_len = 0;

char* dataSend(void){
	char* temp = (char*)os_zalloc(128+1);
	os_memcpy(temp, moduleID, 17);
	data_len = 17;
	return temp;
}
void wifiConnectCb(uint8_t status)
{
	if(status == STATION_GOT_IP){
		MQTT_Connect(&mqttClient);
	} else {
		MQTT_Disconnect(&mqttClient);
	}
}
void mqttConnectedCb(uint32_t *args)
{
	MQTT_Client* client = (MQTT_Client*)args;
	INFO("MQTT: Connected\r\n");
	MQTT_Subscribe(client, "/topic", 0);

	MQTT_Publish(client, "/topic", "18:fe:34:f8:b6:84", 17, 0, 0);


}

void mqttDisconnectedCb(uint32_t *args)
{
	MQTT_Client* client = (MQTT_Client*)args;
	INFO("MQTT: Disconnected\r\n");
}

void mqttPublishedCb(uint32_t *args)
{
	MQTT_Client* client = (MQTT_Client*)args;
	INFO("MQTT: Published\r\n");
}

void mqttDataCb(uint32_t *args, const char* topic, uint32_t topic_len, const char *data, uint32_t data_len)
{
	char *topicBuf = (char*)os_zalloc(topic_len+1),
			*dataBuf = (char*)os_zalloc(data_len+1);

	MQTT_Client* client = (MQTT_Client*)args;

	os_memcpy(topicBuf, topic, topic_len);
	topicBuf[topic_len] = 0;

	os_memcpy(dataBuf, data, data_len);
	dataBuf[data_len] = 0;

	INFO("Receive topic: %s, data: %s \r\n", topicBuf, dataBuf);
	os_free(topicBuf);
	os_free(dataBuf);
}

void user_rf_pre_init(void)
{
}

void user_init(void)
{
	char temp[64];
	char moduleID[17];
	uart_init(BIT_RATE_115200, BIT_RATE_115200);
	os_delay_us(1000000);

	if(BUTTON_SETUP)
	{
		INFO("\r\nsoft ap mode - Configure wifi");
		if(!wifi_set_opmode(SOFT_AP)){
			INFO("\r\nset mode Soft-Ap fail");
		}
	}
	else{
		INFO("\r\nstation mode");
		if(!wifi_set_opmode(STATION)){
			INFO("\r\nset mode station fail");
		}
	}

	if(wifi_get_macaddr(STATION_IF,bssid)){
		os_sprintf(moduleID, "\""MACSTR"\"\r\n",
		               MAC2STR(bssid));
		INFO("\r\nMac Address: %s",moduleID);
	}
	else
		INFO("\r\nGet Mac Address fail");


	//check size flash
	INFO("\r\nsize map: %d",system_get_flash_size_map());
	//set size buffer for binary certificate
	espconn_secure_set_size(0x01,5120);
	os_delay_us(100);
	//check size buffer of binary certificate
	size_buf = espconn_secure_get_size(0x01);
	INFO("\r\nsize buf: %d",size_buf);
	//store binary ca certificate at sector 0x3a
	if(espconn_secure_ca_enable(0x01, 0x3a))
		INFO("\r\nsecure ca enable: success");
	//store binary cert and private key at sector 0x3b
	if(espconn_secure_cert_req_enable(0x01, 0x3b))
		INFO("\r\nsecure cert req enable: success");

	CFG_Load();
	wifi_set_phy_mode (PHY_MODE_11B);
	WIFI_Connect(sysCfg.sta_ssid, sysCfg.sta_pwd, wifiConnectCb);

	MQTT_InitConnection(&mqttClient, sysCfg.mqtt_host, sysCfg.mqtt_port, sysCfg.security);
	MQTT_InitClient(&mqttClient, sysCfg.device_id, sysCfg.mqtt_user, sysCfg.mqtt_pass, sysCfg.mqtt_keepalive, 1);
	MQTT_InitLWT(&mqttClient, "/lwt", "offline", 0, 0);

	MQTT_OnConnected(&mqttClient, mqttConnectedCb);
	MQTT_OnDisconnected(&mqttClient, mqttDisconnectedCb);
	MQTT_OnPublished(&mqttClient, mqttPublishedCb);
	MQTT_OnData(&mqttClient, mqttDataCb);


	INFO("\r\nSystem started ...\r\n");
}
