#ifndef _USER_CONFIG_H_
#define _USER_CONFIG_H_

#define CFG_HOLDER	0x00FF561	/* Change this value to load default configurations */
#define CFG_LOCATION	0x3C	/* Please don't change or if you know what you doing */
#define CLIENT_SSL_ENABLE

#define STATION_IF	0x00
#define SOFTAP_IF	0x01
#define STATION		0x01
#define SOFT_AP		0x02
#define STATION_AP	0x03
#define BUTTON_SETUP	0
/*DEFAULT CONFIGURATIONS*/

#define MQTT_HOST			"AKBULV00LSOQR.iot.us-east-1.amazonaws.com" //or "mqtt.yourdomain.com"
#define MQTT_PORT			8883//1880//
#define MQTT_BUF_SIZE		1024
#define MQTT_KEEPALIVE		120	 /*second*/

#define MQTT_CLIENT_ID		"18FE34F8B684"
#define MQTT_USER			""
#define MQTT_PASS			""

#define STA_SSID "peace"//"iTLabs4U_OutRoom"//
#define STA_PASS "KHONGCOPASS123"//"khongcopassnnn"//
#define STA_TYPE AUTH_WPA2_PSK

#define MQTT_RECONNECT_TIMEOUT 	5	/*second*/

#define DEFAULT_SECURITY	1
#define QUEUE_BUFFER_SIZE		 		2048

#define PROTOCOL_NAMEv31	/*MQTT version 3.1 compatible with Mosquitto v0.15*/
//PROTOCOL_NAMEv311			/*MQTT version 3.11 compatible with https://eclipse.org/paho/clients/testing/*/
#endif
