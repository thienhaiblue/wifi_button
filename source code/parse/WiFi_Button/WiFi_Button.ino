#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include <ESP8266WebServer.h>

#include "Parse.h"

//#define TEST
#define CONFIG  0
#define NORMAL  1
unsigned char mode = CONFIG;
const char* ssid = "peace";
const char* password = "KHONGCOPASS123";
char* objectId = new char[10];

const char *ssid_ap = "config_me";
const char *password_ap = "12345678";

ESP8266WebServer server(80);

const int buttonPin = 5;     // the number of the pushbutton pin
int buttonState = 0;         // variable for reading the pushbutton status
int press_time = 0;
unsigned char press_wakeup = 0;
char buf[10];
byte val;

void createObject() {
  
  sprintf(buf, "%d", press_time);  
  Serial.println("create object");
  ParseObjectCreate create;
  create.setClassName("IoTDemo");
  create.add("moduleID", "18FE34F8B684");
  create.add("mobileID", "12345");
  create.add("presstimes",buf);
  {
      ParseResponse createResponse = create.send();
      delay(200);
      strcpy(objectId, createResponse.getString("objectId"));      
      createResponse.close();
  }
}
#ifdef TEST
void updateObject(){  
  sprintf(buf, "%d", press_time);
  Serial.println("update...");
  ParseObjectUpdate update;
  update.setClassName("IoTDemo");
  update.setObjectId(objectId);
  update.add("presstimes", buf);
  {
      ParseResponse updateResponse = update.send();
      updateResponse.close();
  }
  
}

void handleRoot() {
  server.send(200, "text/html", "<h1>You are connected</h1>");
}
#endif
void setup() {
  Serial.begin(115200);
  Serial.println();
  pinMode(buttonPin, INPUT);
  EEPROM.begin(512);

  #ifdef TEST
  Serial.print("\r\nWaiting Press Button ...");
  delay(2000);
  buttonState = digitalRead(buttonPin);
   
  if (buttonState == LOW) {
    Serial.print("\r\nPress Starting...");
    delay(2000);
    while(press_wakeup<2){
       buttonState = digitalRead(buttonPin);
      if (buttonState == LOW){
        press_wakeup++;
        Serial.print(press_wakeup);
      }
      delay(100);
    }
  }
  if(press_wakeup<2){
    Serial.print("Normal mode\r\n");
  }
  else{
    Serial.print("Config mode\r\n");
    //Create wifi host
    Serial.print("Configuring access point...");
  /* You can remove the password parameter if you want the AP to be open. */
    WiFi.
    WiFi.softAP(ssid_ap);
  
    IPAddress myIP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(myIP);
    server.on("/", handleRoot);
    server.begin();
    Serial.println("HTTP server started");
  }
 #endif 
  Serial.print("connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  Parse.begin("kk3MBQNWligPEUjHcvUXimOhlUgns9ELYWwU1BI3", "sCdL8jGUF981DP5UFDU2OjbzvxRqJBD3diTcBhNs");
  delay(100);
  eeprom_read();
  createObject();
  Serial.println("\r\nWaiting for next ...");
 
}


void loop() {
  buttonState = digitalRead(buttonPin);
  if (buttonState == LOW) {
    press_time++;
    eeprom_write(press_time);
    //eeprom_read();
    createObject();     
    delay(100);
    Serial.println("\r\nWaiting for next ..."); 
      
  } 
  delay(200);
  #ifdef TEST
  server.handleClient();
  #endif
}

void eeprom_write(int val){
  EEPROM.write(0x01, val/256);
  EEPROM.write(0x02, val%256);
  EEPROM.commit();
}
void eeprom_read(void){
  press_time = (int) EEPROM.read(0x01) * 256 + (int)EEPROM.read(0x02);
  Serial.print("\r\nPress time:");
  Serial.print(press_time);
}

